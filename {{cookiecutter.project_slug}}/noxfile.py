"""Nox configuration."""

import nox
from nox.sessions import Session

nox.options.sessions = "lint", "mypy"

locations = "src", "tests", "noxfile.py"
mypy_stubs = ("types-" + p for p in ["click"])


@nox.session
def lint(session: Session) -> None:
    """Lint using Flake8."""
    args = session.posargs or locations
    session.install(
        "flake8",
        "flake8-black",
        "flake8-annotations",
        "flake8-docstrings",
        "flake8-import-order",
    )
    session.run("flake8", *args)


@nox.session
def mypy(session: Session) -> None:
    """Type-check using mypy."""
    args = session.posargs or locations
    session.install("mypy", *mypy_stubs)
    session.run("mypy", *args)


@nox.session
def black(session: Session) -> None:
    """Format using Black."""
    args = session.posargs or locations
    session.install("black")
    session.run("black", *args)


@nox.session(venv_backend="conda")
def tests(
    session: Session,
) -> None:
    """Run test suite."""
    # https://github.com/theacodes/nox/issues/260
    session.run(
        "conda",
        "env",
        "update",
        "--prefix",
        session.virtualenv.location,
        "-f",
        "environment.yml",
        "--prune",
        silent=True,
    )
    session.run("pytest")
