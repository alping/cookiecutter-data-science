import setuptools

setuptools.setup(
    name="src",
    version="0.0.1",
    author="{{cookiecutter.author_name}}",
    author_email="{{cookiecutter.author_email}}",
    description="{{cookiecutter.description}}",
    packages=setuptools.find_packages(),
    entry_points="""
        [console_scripts]
        src=src.cli:src
    """
)
