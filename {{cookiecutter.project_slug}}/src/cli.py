"""Command line interface for project."""

import click


@click.group()
def src() -> None:
    """CLI group for commands."""
    pass


@src.command(short_help="Run a flow")
def run() -> None:
    """Run flow."""
    # TODO: Set up run function
    print("Set up run function in src/cli.py")


if __name__ == "__main__":
    run()
