"""Tests for CLI."""

import click.testing

from src import cli


def test_run_succeeds() -> None:
    """Test CLI run function."""
    runner = click.testing.CliRunner()
    result = runner.invoke(cli.run)
    assert result.exit_code == 0
