# Cookiecutter - Data Science

Requires [Conda](https://docs.conda.io/) / [Miniconda](https://docs.conda.io/en/latest/miniconda.html).

```shell
# Install requirements
conda install cookiecutter mamba nox -n base -c conda-forge
# Create project from template
cookiecutter gl:alping/cookiecutter-data-science
# Create and activate Conda environment
cd <project_name>
mamba env create -f environment.yml
mamba activate <env_name>
```

## Dev Tools

- [`Mamba`](https://mamba.readthedocs.io/)
- [`Nox`](https://nox.thea.codes/)

## Dependencies

- [`click`](https://click.palletsprojects.com/)
- [`dotenv`](https://saurabh-kumar.com/python-dotenv/)
- [`pandas`](https://pandas.pydata.org/docs/)
- [`matplotlib`](https://matplotlib.org/stable/index.html)
- [`prefect (orion)`](https://orion-docs.prefect.io/)

## Dev Dependencies

- [`black`](https://black.readthedocs.io/)
- [`flake8`](https://black.readthedocs.io/)
- [`mypy`](https://black.readthedocs.io/)
- [`pytest`](https://black.readthedocs.io/)
- [`ipython`](https://ipython.readthedocs.io/en/stable/)
- [`ipykernel`](https://ipython.readthedocs.io/en/stable/)

## Local Packages

- `src` as local editable package

## Formatting

Formatting using `black`.

## TODO

- [ ] Add Git Commit hook

## References

- Partly inspired by [Hypermodern Python](https://medium.com/@cjolowicz/hypermodern-python-d44485d9d769)
